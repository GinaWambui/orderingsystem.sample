<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: grey;
                background-image: url("/css/Pictures/jollof-rice.jpg");
                background-size:cover;

                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            /* Bordered form */
            form {
               border: 3px solid #f1f1f1;
               background-color: #C0C0C0;
            }

            /* Full-width inputs */
            input[type=text], input[type=password] {
               width: 100%;
               padding: 12px 20px;
               margin: 8px 0;
               display: inline-block;
               border: 1px solid #ccc;
               box-sizing: border-box;
            }

            /* Set a style for all buttons */
            button {
               background-color: #FF4500;
               color: white;
               padding: 14px 20px;
               margin: 8px 0;
               border: none;
               cursor: pointer;
               width: 100%;
            }

            /* Add a hover effect for buttons */
            button:hover {
               opacity: 0.8;
            }

            /* Extra style for the cancel button (red) */
            .cancelbtn {
               width: auto;
               padding: 10px 18px;
               background-color: #f44336;
            }

            /* Center the avatar image inside this container */
            .imgcontainer {
               text-align: center;
               margin: 24px 0 12px 0;
            }

            /* Avatar image */
            img.avatar {
               width: 40%;
               border-radius: 50%;
            }

            /* Add padding to containers */
            .container {
               padding: 16px;
            }

            /* The "Forgot password" text */
            span.psw {
               float: right;
               padding-top: 16px;
            }

            @media screen and (max-width: 300px) {
               span.psw {
                   display: block;
                   float: none;
               }
               .cancelbtn {
                   width: 100%;
               }
            }
                    </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">

            <form class="" action="{{URL::to('/logs')}}" method="post">
              <div class="title m-b-md">
                  Login
              </div>
              <input type="text" name="Name" placeholder="Enter Name" value="">
              <br><br>
              <input type="password" name="Password" encrypt="bcrypt" salt="abc..." rounds="4" pattern=".{6,}" placeholder="Enter Password" value="">
              <br><br>
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <br><br>
              <button type="submit" name="button">Login</button>
              <a href="register">No Account. Click to Register</a>
              <br><br>
              <a href="managerlogin">Login as manager</a>

            </form>

            </div>
        </div>
    </body>
</html>
