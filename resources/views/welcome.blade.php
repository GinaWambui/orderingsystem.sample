<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                background-image: url("/css/Pictures/pilau.jpg");
                background-size:cover;
                color: #ffff;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            link a {
                padding: 0 25px;
                font-size: 16px;
                color: #000080;
                font-weight: 1000;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform:lowercase;
            }

            .m-b-md {
                margin-bottom: 30px;
                color:white;
            }
            .comment{
              font-size: 25px;
              font-family: 'Didot', serif;
              color: brown;
            }
            /* Bordered form */
            form {
               border: 3px solid #f1f1f1;
               background-color: #FF4500;
               opacity: 0.9;
               align:center;          }
             }


            /* Add padding to containers */
            .container {
               padding: 16px;
            }

            /* The "Forgot password" text */
            span.psw {
               float: right;
               padding-top: 16px;
            }

            @media screen and (max-width: 300px) {
               span.psw {
                   display: block;
                   float: none;
               }
               .cancelbtn {
                   width: 100%;
               }

}
        </style>
    </head>
    <body>
      <div class="flex-center position-ref full-height">

                  <div align= "Center"><form class="" action="{{URL::to('/logs')}}" method="post">
                    <div class="title m-b-md">
                         MSAFIRI ORDER PAGE
                    </div>
                    <div class="title comment">
                      We deliver anywhere within the CBD
                    </div>
                  </br>
                    <a href="login">Click to login</a>
                  </br>
                    <a href="register">No Account? Click to Register</a>
                  </form>
                </div>
    </body>
</html>
