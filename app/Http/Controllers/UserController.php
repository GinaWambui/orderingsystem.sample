<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Hash;

class UserController extends Controller
{
  public function store(request $request){
    //print_r($request->input());
    $name=$request->input('Name');
    $email=$request->input('Email');
    $password=$request->input('Password');
    $hash=Hash::make($password);
    $info= DB::insert('insert into users(id,name,email,password) values(?,?,?,?)' ,[null,$name,$email,$hash]);
      return Redirect::to('login');

    }

  public function logs(request $request){
    print_r($request->input());

    $name=$request->input('Name');
    $password=$request->input('Password');
    $data= DB::select('select id from users where name=? and password =?' ,[$name,$password]);

    if(count($data))
    {
      echo "Logged On Successfully";
      return Redirect::to('menu');

    }
   else{
      echo "Login Attempt Failed,Try Again";

    }

  }

}
?>
